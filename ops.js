// operators
const values = []
for (let i = 0; i < 15; i++) {
  values[i] = (Math.random() * 100)
}

// transpose manually
const shapeA = [5, 3]
const shapeB = [3, 5]

const a = tf.tensor2d(values, shapeA, 'int32')
const b = tf.tensor2d(values, shapeA, 'int32') // same shape of const a if you using transpose() function

// or use transpose() function
const b_t = tf.transpose(b)


//const c = a.add(b)
const c = a.matMul(b_t)  // matrices multiplication

c.print()

// https://js.tensorflow.org/api/latest/#Operations
// https://js.tensorflow.org/api/latest/#Operations-Matrices