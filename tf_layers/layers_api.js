// create model
const model = tf.sequential()

// config the layers
const hidden = tf.layers.dense({
  units: 4, // number of nodes
  inputShape: 2,
  activation: 'sigmoid'
})

const output = tf.layers.dense({
  units: 1,
  // the inputShape is inferred from the previews (hidden) layer
  activation: 'sigmoid'
})

// add layers to model
model.add(hidden)
model.add(output)

//compile the model
model.compile({
  optimizer: tf.train.sgd(0.1), // this is a gradient descent
  loss: tf.losses.meanSquaredError
})

// training the model
const xs = tf.tensor2d([
  [0, 0],
  [0.5, 0.5],
  [1, 1]
])

const ys = tf.tensor2d([
  [1],
  [0.5],
  [0]
])

const train = async () => {
  for (let i = 0; i < 1000; i++) {
    const fitConfig = {
      shuffle: true,
      epochs: 10
    }
    const response = await model.fit(xs, ys, fitConfig)
    console.log(response.history.loss[0])
  }
}

train().then(() => {
  let outputs = model.predict(xs)
  outputs.print()
  console.log('training is complete')
})

// create the tensor
/* const inputs = tf.tensor2d([
  [0.25, 0.92]
]) */















// https://js.tensorflow.org/api/latest/#sequential
// https://js.tensorflow.org/api/latest/#layers.dense
// https://js.tensorflow.org/api/latest/#losses.cosineDistance
// https://js.tensorflow.org/api/latest/#tf.LayersModel.predict