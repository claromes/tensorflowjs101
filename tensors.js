//first tensor
const data = tf.tensor([0, 0, 127, 255, 100, 50, 24, 54], [2, 2, 2])
console.log(data.toString())

/*******************/

const values = []
for (let i = 0; i < 30; i++) {
  values[i] = (Math.random() * 100)
}

const shape = [2, 5, 3]

// generic tensor
const tensor = tf.tensor(values, shape, 'int32')

// 3 dimensional tensor
const tensor3d = tf.tensor3d(values, shape, 'int32')

console.log(tensor.toString())
console.log(tensor3d.toString())

// https://js.tensorflow.org/api/latest/#Tensors