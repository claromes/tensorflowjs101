const values = []
for (let i = 0; i < 30; i++) {
  values[i] = (Math.random() * 100)
}

const shape = [2, 5, 3]

const tensor3d = tf.tensor3d(values, shape, 'int32')

// variables
const vtensor3d = tf.variable(tensor3d)
console.log(vtensor3d)

// access the data
//tensor3d.data().then((finalData) => console.log(finalData))

// or
console.log(tensor3d.dataSync())

//get a data by index
// https://stackoverflow.com/questions/52858707/tensorflow-js-problem-error-number-of-coordinates-in-get-must-match-the-ran
console.log(tensor3d.dataSync()[0])

// https://js.tensorflow.org/api/latest/#variable