setInterval(() => {
  tf.tidy(() => {
    const values = []
    for (let i = 0; i < 150000; i++) {
      values[i] = (Math.random() * 100)
    }
    const shape = [500, 300]

    const a = tf.tensor2d(values, shape, 'int32')
    const b = tf.tensor2d(values, shape, 'int32')

    const b_t = tf.transpose(b)

    const c = a.matMul(b_t)

    c.print()

    a.dispose()
    b.dispose()
    c.dispose()
    b_t.dispose()
  })
  console.log(tf.memory().numTensors)
}, 2000)

// sudo watch nvidia-smi
// https://js.tensorflow.org/api/latest/#Performance-Memory